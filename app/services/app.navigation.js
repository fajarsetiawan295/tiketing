import * as React from 'react'
import {
    Image,
} from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch'
import { Transition } from 'react-native-reanimated';
import Icon from './../component/icons'
import { login, register, splashScreen } from '../screen/auth';
import * as homeItem from './../screen/home';
import * as ticket from './../screen/Ticket';
import * as tables from './../screen/Tables';
import * as tabbottom from './../screen/tab.bottom'

import demo from './../screen/demo'

const img = require('./../assets/image/add.png')

const TabBottom = createMaterialBottomTabNavigator({
    Home: {
        screen: tabbottom.home,
        navigationOptions: {
            tabBarLabel: 'Home',
            tabBarIcon: ({ tintColor }) => (
                <Icon.MaterialIcons name='home' color={tintColor} size={25} />
            )
        }
    },
    Profile: {
        screen: tabbottom.profile,
        navigationOptions: {
            tabBarLabel: 'Profile',
            tabBarIcon: ({ tintColor }) => (
                <Icon.MaterialIcons name='account-circle' color={tintColor} size={25} />
            )
        }
    }
}, {
    initialRouteName: 'Home',
    activeColor: '#B08158',
    inactiveColor: 'black',
    barStyle: {
        activeColor: '#B08158',
        backgroundColor: 'white'
    }
})


const Auth = createStackNavigator({
    Login: login,
}, {
    initialRouteName: 'Login',
    headerMode: 'none'
})

const HomeNavigator = createStackNavigator({
    Demo: demo,
    Home: tabbottom.home,
    Profile: tabbottom.profile,
    TabBottom: TabBottom,
    Register: register,
    CreateTicket: ticket.createTicket,
    ExportList: ticket.ExportList,
    GetHistoryTicket: tables.ViewTables,
    UpdateTables: tables.UpdateTables,
    ChatTables: tables.ChatTables,

}, {
    initialRouteName: 'TabBottom',
    headerMode: 'none'
})



const MainNavigator = createAnimatedSwitchNavigator({
    SplashScreen: splashScreen,
    Auth: Auth,
    HomeNavigator: HomeNavigator

},
    {
        transition: (
            <Transition.Together>
                <Transition.Out
                    type="slide-right"
                    durationMs={400}
                    interpolation="easeIn"
                />
                <Transition.In type="slide-left" durationMs={400} />
            </Transition.Together>
        ),
    })



export default createAppContainer(MainNavigator)