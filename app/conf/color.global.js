const colors = {
    colorsSecondary: '',
    colorsLight: '#FFFFFF',
    colorsDark: '#1C1819',
    colorsBorder: '#E0E0E0',
    colorsLightDark: '#C2C2C226',
    colorsRed: '#C92A2A',
    colorBlack: '#000000',
    colorGrey: '#474747',
    colorPLATINUM: '#E5E4E2',
    colorYellow: '#FBF7E1',
    colorBrown: '#EB8934',
    GreenYellow: '#ADFF2F',
    Greeen: '#32CD32',

    YellowButton: '#b9d7ea',
    IconColor: '#769fcd'
}


export default colors