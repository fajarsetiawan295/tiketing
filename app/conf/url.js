export const url = 'https://tiketing.herokuapp.com';

export const url_api = url + '/api';

export const api = {

    login: url_api + "/auth/login",
    Register: url_api + "/auth/signup",
    profile: url_api + "/auth/user",
    GetTitle: url_api + "/auth/Data-Get/title",
    CreateTicket: url_api + "/tiket/Create",
    GetTicket: url_api + "/tiket/Get-Tiket",
    UpdateTicket: url_api + "/tiket/Upadte-Status",
    GetDasboard: url_api + "/tiket/Data-Dasbord",
    GetDepartemen: url_api + "/auth/Data-Get/departemen",
    Getlocation: url_api + "/auth/Data-Get/loct",
    GetCategory: url_api + "/auth/Data-Get/category",

    getchat: url_api + "/chat/Get-Chat/",
    chatcreate: url_api + "/chat/Create"

};
