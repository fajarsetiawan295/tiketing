import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux'


class splashScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    UNSAFE_componentWillMount() {
        const { navigation } = this.props
        if (this.props.isLogged) {
            navigation.navigate('Home')
        }
        else {
            navigation.navigate('Login')
        }
    }

    render() {
        return (
            <View />
        );
    }
}


const mapStateToProps = ({ AuthReducer }) => {
    const { isLogged } = AuthReducer
    return { isLogged }
}

export default connect(mapStateToProps, null)(splashScreen);
