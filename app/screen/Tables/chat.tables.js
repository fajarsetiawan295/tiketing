import React, { Component } from 'react';
import {
    Alert,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    ScrollView,
    StyleSheet,
    Image,
    RefreshControl,
    ActivityIndicator
} from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { Dropdown } from 'react-native-material-dropdown';
import { colors } from './../../conf';
import { post_services, Get_services } from './../../services';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    dataUser,
    isLogged
} from './../../redux/actions';
import DropdownAlert from 'react-native-dropdownalert';
import { api } from './../../conf/url';
import { GiftedChat } from 'react-native-gifted-chat'



class chattables extends Component {
    constructor(props) {
        super(props);
        this.state = {
            urgent: false,
            isi: '',
            Getcategory: [],
            messages: [],
        };
    }


    handleLogin(x) {
        this.onLoginPress(x)
    }

    onLoginPress(x) {
        const state = this.state;
        const { navigation } = this.props;
        const data = new FormData();
        data.append('no_ticket', navigation.getParam('id'));
        data.append('details_tiket', x);
        data.append('status', 'unread');

        post_services(this.props.token, data, api.chatcreate).then((rspons) => {
            this.setState({ animating: false });
            console.log(rspons);
            if (rspons.status == 200) {
                this.onGetChat(navigation.getParam('id'));
            } else if (response.status == 401) {
                console.log(response)
                this.props.navigation.navigate('Login')
            } else {
                this.dropDownAlertRef.alertWithType('error', 'Error', rspons.data.errors);
            }
        });
    }

    onGetChat(x) {
        try {
            Get_services(this.props.token, api.getchat + x)
                .then((response) => {
                    if (response.status == 200) {
                        this.setState({ messages: response.data.data })
                        console.log(response)
                    }
                    else if (response.status == 401) {
                        console.log(response)
                        this.props.navigation.navigate('Login')
                    }
                    else {
                        console.log(response)
                    }
                })
        } catch (err) {
            console.log(err)

        }

    }
    onSend(messages = []) {
        this.setState({ isi: messages[0]['text'] })
        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages),
        }))
        this.handleLogin(messages[0]['text']);
    }


    UNSAFE_componentWillMount() {
        const { navigation } = this.props;
        this.onGetChat(navigation.getParam('id'))
    }
    refreshContent() {
        this.setState({ refresh: true })
        const { navigation } = this.props;
        this.onGetChat(navigation.getParam('id'))
        this.setState({ refresh: false })
    }
    render() {
        const { navigation } = this.props;
        const state = this.state;
        const img = require('./../../assets/image/falcon2.png')
        return (
            <>
                <ScrollView
                    refreshControl={
                        <RefreshControl refreshing={this.state.refresh} onRefresh={() => this.refreshContent()} />
                    }
                    showsVerticalScrollIndicator={false}>

                </ScrollView>

                <GiftedChat
                    messages={this.state.messages}
                    onSend={messages => this.onSend(messages)}
                    user={{
                        _id: this.props.infoUser.id,
                    }}
                />
            </>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.colorsLight,
        alignItems: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

const mapStateToProps = ({
    AuthReducer,
    network,
}) => {
    const { token, infoUser } = AuthReducer;
    const { isConnected } = network;
    return {
        isConnected,
        token,
        infoUser,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            dataUser,
            isLogged
        },
        dispatch,
    );
};


export default connect(mapStateToProps, mapDispatchToProps)(chattables);