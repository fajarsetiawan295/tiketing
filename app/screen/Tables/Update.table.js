import React, { Component } from 'react';
import {
    Alert,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    ScrollView,
    StyleSheet,
    Image,
    CheckBox,
    ActivityIndicator
} from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { Dropdown } from 'react-native-material-dropdown';
import { colors } from './../../conf';
import { post_services, Get_services } from './../../services';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    dataUser,
    isLogged
} from './../../redux/actions';
import DropdownAlert from 'react-native-dropdownalert';
import { api } from './../../conf/url';


class updatetables extends Component {
    constructor(props) {
        super(props);
        this.state = {
            urgent: false,
            category: '',
            description: '',
            status: '',
            Getcategory: [],
        };
    }


    handleLogin() {
        this.onLoginPress()
    }

    onLoginPress() {
        this.setState({ animating: true });
        const state = this.state;
        const { navigation } = this.props;

        const data = new FormData();
        data.append('deskripsi', state.description);
        data.append('status', state.status);
        data.append('tiket_id', navigation.getParam('id_tabungan'));

        post_services(this.props.token, data, api.UpdateTicket).then((rspons) => {
            this.setState({ animating: false });
            console.log(rspons);
            if (rspons.status == 200) {
                navigation.navigate('Home');
            } else if (response.status == 401) {
                console.log(response)
                this.props.navigation.navigate('Login')
            }
            else {
                this.dropDownAlertRef.alertWithType('error', 'Error', rspons.data.errors);
            }
        });
    }
    render() {
        const { navigation } = this.props;
        const state = this.state;
        const img = require('./../../assets/image/falcon2.png')
        let data = [
            {
                value: 'Done',
                id: 1
            },
            {
                value: 'On Progres',
                id: 2
            },
        ]

        return (
            <View style={styles.container}>
                {this.state.animating == true ?
                    <View>
                        <ActivityIndicator
                            animating={this.state.animating}
                            color='#bc2b78'
                            size="large"
                            style={styles.activityIndicator} />
                        <TouchableOpacity
                            onPress={() => this.setState({ animating: false })}
                            style={styles.buttoncancel}>
                            <Text style={styles.textButton}>Cancel</Text>
                        </TouchableOpacity>
                    </View>
                    :
                    <View style={styles.container}>
                        <ScrollView
                            contentContainerStyle={{ alignItems: 'center' }}
                            showsVerticalScrollIndicator={false}>

                            <View style={styles.headerLogo}>
                                <Image source={img} style={styles.imageLogo} />
                            </View>
                            <View style={styles.cardFormInput}>
                                <Text
                                    style={{
                                        color: '#626270',
                                        fontFamily: 'ProximaNova',
                                        fontSize: wp('4.5%'),
                                    }}>Status</Text>
                                <Dropdown
                                    placeholder={'Status'}
                                    lineWidth={0}
                                    containerStyle={styles.formText}
                                    data={data}
                                    valueExtractor={({ value }) => value}
                                    labelExtractor={({ value }) => value}
                                    onChangeText={(x) => this.setState({ status: x })}
                                    textColor="#515151"
                                    itemColor="black"
                                    fontSize={wp('4')}
                                    itemTextStyle={{ fontFamily: 'ProximaNova', }}
                                    itemPadding={5}
                                    labelHeight={-3}
                                    dropdownOffset={{ 'top': wp('22%') }}
                                />
                            </View>
                            <View style={styles.cardFormInput}>
                                <Text style={styles.textFormTitle}>Description</Text>
                                <TextInput
                                    value={state.description}
                                    style={{

                                        height: hp('20'),
                                        width: wp('80%'),
                                        paddingLeft: 10,
                                        borderRadius: 7,
                                        borderWidth: 1,
                                        borderColor: colors.YellowButton,
                                        color: colors.colorBlack,
                                        fontFamily: 'ProximaNova',
                                    }}
                                    onChangeText={(x) => this.setState({ description: x })}
                                />
                            </View>
                            <TouchableOpacity
                                onPress={() => this.handleLogin()}
                                style={styles.button}>
                                {
                                    state.isLoading == true ?
                                        <ActivityIndicator color={'red'} size='small' />
                                        :
                                        <Text style={styles.textButton}>Submit</Text>
                                }

                            </TouchableOpacity>
                        </ScrollView>
                    </View>
                }
                <DropdownAlert ref={(ref) => (this.dropDownAlertRef = ref)} closeInterval={5000} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.colorsLight,
        alignItems: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textFormTitle: {
        color: '#626270',
        fontFamily: 'ProximaNova',
        fontSize: wp('4.5%'),
    },
    cardFormInput: {
        width: wp('80%'),
        marginTop: hp('2%'),
    },
    cardFormInputemail: {
        width: wp('80%'),
        // marginTop: hp('1%'),
    },
    formText: {
        height: hp('6'),
        width: wp('80%'),
        paddingLeft: 10,
        borderRadius: 7,
        borderWidth: 1,
        borderColor: colors.YellowButton,
        color: colors.colorBlack,
        fontFamily: 'ProximaNova',
        // elevation: 1,
        // opacity: 0.99
    },
    button: {
        width: wp('80%'),
        marginTop: hp('4%'),
        borderRadius: 7,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.YellowButton,
        height: hp('6%'),
    },
    buttonregister: {
        width: wp('80%'),
        marginTop: hp('5%'),
        borderRadius: 7,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: colors.YellowButton,
        // backgroundColor: colors.YellowButton,
        height: hp('6%'),
    },
    checkboxContent: {
        flexDirection: 'row',
        width: wp('80'),
        alignItems: 'center',
        marginTop: hp('1'),
    },
    textButton: {
        fontFamily: 'ProximaNova',
        color: colors.colorsLight,
        fontSize: wp('5%'),
    },
    textQuestionAccount: {
        fontFamily: 'ProximaNova',
        color: '#626270',
        fontSize: wp('4.5%'),
    },
    imageLogo: {
        width: wp('70%'),
        height: hp('20%'),
        resizeMode: 'cover',
    },
    activityIndicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 80
    },
    imageLogo: {
        width: wp('70%'),
        height: hp('30%'),
        resizeMode: 'cover'
    },
    headerLogo: {
        // backgroundColor: '#1C1819',
        width: wp('100%'),
        height: hp('30'),
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: hp('3%'),
        marginBottom: hp('2%')
    },


    textUserPayment: {
        color: colors.colorBlack,
        fontFamily: 'ProximaNovaSemiBold',
        fontSize: 12,
    },

});

const mapStateToProps = ({
    AuthReducer,
    network,
}) => {
    const { token, infoUser } = AuthReducer;
    const { isConnected } = network;
    return {
        isConnected,
        token,
        infoUser,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            dataUser,
            isLogged
        },
        dispatch,
    );
};


export default connect(mapStateToProps, mapDispatchToProps)(updatetables);