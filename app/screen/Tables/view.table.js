import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Header, Saldo, Loading, valueMoneyFormat } from './../../component'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { Dropdown } from 'react-native-material-dropdown';
import Icon from './../../component/icons'
import colors from '../../conf/color.global';
import { Get_services } from './../../services';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    dataUser,
} from './../../redux/actions';
import moment from 'moment';
import DropdownAlert from 'react-native-dropdownalert';
import { api } from './../../conf/url';



class viewtables extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lod: [],
            dataid: '',
            animating: true,
        };
    }

    onGetHistory() {
        try {
            Get_services(this.props.token, api.GetTicket).then(
                (response) => {
                    console.log('masuk sini');
                    if (response.status == 200) {
                        this.setState({
                            lod: response.data.data,
                            animating: false
                        })
                        console.log('ini user', response.data.data);
                    } else if (response.status == 401) {
                        this.props.navigation.navigate('Login');
                    }
                    else {
                        console.log(response);
                    }
                },
            );
        } catch (err) {
            console.log('masuk sini', err);
        }
    }

    UNSAFE_componentWillMount() {
        this.onGetHistory();
    }

    getid(x) {
        this.setState({ dataid: x })
    }

    render() {
        let { navigation } = this.props

        return (
            <View style={styles.container}>

                {
                    this.state.animating == true ?
                        <View style={{
                            flex: 1,
                            alignItems: 'center',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            <ActivityIndicator
                                animating={this.state.animating}
                                color='#bc2b78'
                                size="large"
                                style={styles.activityIndicator} />
                        </View>
                        :
                        <View style={styles.container}>
                            <Header
                                onPress={() => navigation.navigate('Home')}
                                title='History Tiketing'
                            />
                            <ScrollView showsVerticalScrollIndicator={false}>
                                {
                                    this.state.lod.map((item, key) => {
                                        return (
                                            <View>
                                                <TouchableOpacity
                                                    onPress={() => this.getid(item.id)}
                                                    key={key} style={styles.containerListView}>
                                                    <View style={styles.subContainerListView}>
                                                        <View>
                                                            <View style={styles.containerCyrcleIcon}>
                                                                <Icon.Foundation name={'ticket'} color='#1C1819' size={25} />
                                                            </View>
                                                            <View style={{ marginLeft: wp('2%'), marginTop: wp('2%'), justifyContent: "center", alignContent: "center" }}>
                                                                {item.status == 'Open' ?

                                                                    <Text style={styles.textListViewTitle, { color: '#002bd9' }}>Open</Text>
                                                                    :
                                                                    item.status == 'On Progres' ?
                                                                        <Text style={styles.textListViewTitle, { color: 'red' }}>On Progres</Text>
                                                                        :
                                                                        <Text style={styles.textListViewTitle, { color: '#34eb3a' }}>Done</Text>

                                                                }
                                                            </View>
                                                            <Text style={{ color: 'red' }}>
                                                                {item.urgent == 1 ? "Urgent" : null}
                                                            </Text>
                                                        </View>
                                                        <View style={{ width: wp('40%'), marginLeft: wp('2%') }}>
                                                            <View>
                                                                <Text style={styles.textListViewTitle, { marginBottom: hp('1%') }}>{item.user_create.name}</Text>
                                                                <Text style={styles.textListViewTitle}>Subject : </Text>
                                                                <Text style={styles.textListViewTitle, { marginBottom: hp('1%') }}>{item.subject}</Text>
                                                                <Text style={styles.textListViewTitle}>Deskripsi</Text>
                                                                <Text style={styles.textListViewAddress}>{item.deskripsi}</Text>
                                                            </View>
                                                            <View style={{ flexDirection: "row", marginTop: hp('2%') }}>
                                                                <Text style={styles.textListViewMoney}></Text>
                                                                <View style={{ flexDirection: "row", width: wp('50%') }}>
                                                                    {this.props.infoUser.status == 1 ?
                                                                        null
                                                                        :
                                                                        item.status == 'Done' ?
                                                                            null
                                                                            :
                                                                            <TouchableOpacity
                                                                                onPress={() => navigation.navigate('UpdateTables', {
                                                                                    id_tabungan: item.id
                                                                                })}
                                                                                style={styles.buttonZakat}>
                                                                                <Text style={styles.textButtonZakat}>Update Status</Text>
                                                                            </TouchableOpacity>
                                                                    }
                                                                    <TouchableOpacity
                                                                        onPress={() => navigation.navigate('ChatTables', {
                                                                            id: item.id
                                                                        })}
                                                                        style={{
                                                                            marginLeft: wp('2%'),
                                                                            backgroundColor: 'white',
                                                                            height: wp('10%'),
                                                                            width: wp('10%'),
                                                                            justifyContent: 'center',
                                                                            alignItems: 'center',
                                                                            borderRadius: 5,
                                                                            marginTop: hp('3%')

                                                                        }}>
                                                                        <Icon.Entypo name={'chat'} color='#1C1819' size={25} />
                                                                    </TouchableOpacity>
                                                                </View>

                                                            </View>

                                                        </View>
                                                        <View style={{ width: wp('50%') }}>
                                                            <Text style={styles.textListViewDate}>
                                                                {moment(item.created_at).format('DD MMM YYYY HH:mm:ss')}
                                                            </Text>

                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                                {this.state.dataid == item.id ?
                                                    <View style={styles.cardriwayat}>
                                                        <Text style={{
                                                            fontSize: wp('4%'),
                                                            color: colors.colorBlack,
                                                            fontFamily: 'ProximaNovaSemiBold',
                                                        }}>Data History Tiketing</Text>
                                                        {item.status_ticket.map((e, u) => {
                                                            return (
                                                                <View key={u}>
                                                                    <Text style={styles.judulbox}>Status :</Text>
                                                                    <View style={{
                                                                        marginTop: wp('2%'),
                                                                        backgroundColor: 'black',
                                                                        height: wp('0.1%')
                                                                    }}></View>
                                                                    <View style={{ flexDirection: "row", width: wp('30%') }}>

                                                                        {e.status == 'Open' ?
                                                                            <>
                                                                                <View style={styles.ManCyrcleIcon}>
                                                                                    <Icon.FontAwesome5 name={'money-bill-wave'} color='#32CD32' size={25} />
                                                                                </View>
                                                                                <Text style={{
                                                                                    marginTop: wp('5%'),
                                                                                    color: '#32CD32',
                                                                                    width: wp('30%')
                                                                                }}>
                                                                                    Open
                                                                    </Text>
                                                                            </>
                                                                            :
                                                                            item.status == 'On Progres' ?
                                                                                <>
                                                                                    <View style={styles.ManCyrcleIcon}>
                                                                                        <Icon.FontAwesome5 name={'money-bill-wave'} color='red' size={25} />
                                                                                    </View>
                                                                                    <Text style={{
                                                                                        marginTop: wp('5%'),
                                                                                        color: 'red',
                                                                                        width: wp('30%')
                                                                                    }}>
                                                                                        On Progres
                                                                    </Text>
                                                                                </>

                                                                                :
                                                                                <>
                                                                                    <View style={styles.ManCyrcleIcon}>
                                                                                        <Icon.FontAwesome5 name={'money-bill-wave'} color='#34eb3a' size={25} />
                                                                                    </View>
                                                                                    <Text style={{
                                                                                        marginTop: wp('5%'),
                                                                                        color: '#34eb3a',
                                                                                        width: wp('30%')
                                                                                    }}>
                                                                                        Done
                                                                    </Text>
                                                                                </>

                                                                        }


                                                                    </View>
                                                                    <Text style={styles.judulbox}>Deskripsi :</Text>
                                                                    <View style={{
                                                                        backgroundColor: 'black',
                                                                        height: wp('0.1%')
                                                                    }}></View>
                                                                    <View>
                                                                        <Text style={styles.judulboxSaldo}>{e.deskripsi} </Text>
                                                                    </View>


                                                                </View>)
                                                        })}
                                                    </View>
                                                    :
                                                    null

                                                }
                                            </View>

                                        )
                                    })
                                }
                            </ScrollView>
                        </View>
                }
                <DropdownAlert ref={(ref) => (this.dropDownAlertRef = ref)} closeInterval={5000} />
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#FAFAFA',
        flex: 1
    },
    headerContainer: {
        width: wp('100%'),
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: '#70707026',
        height: hp('9%')
    },
    subContainerHeader: {
        flexDirection: 'row',
        width: wp('90%'),
        justifyContent: 'space-between'
    },
    cardriwayat: {
        backgroundColor: '#FFFFFF',
        padding: wp('2%'),
        width: wp('95%'),
        borderRadius: 5,
        borderWidth: 1,
        borderColor: colors.colorsLightDark,
        marginTop: hp('1%'),
        marginBottom: hp('1%')
    },
    textHeader: {
        color: '#1C1819',
        fontSize: wp('5%'),
        fontFamily: 'ProximaNovaSemiBold'
    },
    containerListView: {
        backgroundColor: '#FFD7001A',
        width: wp('95%'),
        borderRadius: 5,
        borderWidth: 1,
        borderColor: colors.colorsLightDark,
        alignItems: 'center',
        marginTop: hp('1%'),
        marginBottom: hp('1%')
    },
    subContainerListView: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: wp('90%'),
        marginVertical: wp('3%')
    },
    judulbox: {

        marginTop: wp('1%'),
        fontSize: wp('3%'),
        color: colors.colorBlack,
        fontFamily: 'ProximaNovaSemiBold',

    },
    judulboxSaldo: {

        marginTop: wp('2%'),
        marginBottom: wp('2%'),
        fontSize: wp('4%'),
        color: colors.colorBlack,
        fontFamily: 'ProximaNovaSemiBold',

    },
    buttonZakat: {
        backgroundColor: 'white',
        height: wp('10%'),
        width: wp('50%'),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        marginTop: hp('3%')
    },
    containerCyrcleIcon: {
        width: wp('15%'),
        height: wp('15%'),
        borderRadius: wp('10%'),
        backgroundColor: colors.colorsLight,
        alignItems: 'center',
        justifyContent: 'center'
    },
    ManCyrcleIcon: {
        width: wp('15%'),
        height: wp('15%'),
        borderRadius: wp('10%'),
        backgroundColor: colors.colorsLight,
        alignItems: 'center',
        justifyContent: 'center',

    },
    textListViewTitle: {
        color: colors.colorBlack,
        fontFamily: 'ProximaNovaSemiBold',
        fontSize: wp('3%')
    },
    textListViewAddress: {
        color: colors.colorBlack,
        fontFamily: 'ProximaNova',
        fontSize: wp('3%')
    },
    textListViewMoney: {
        color: colors.colorsRed,
        fontFamily: 'ProximaNova',
        fontSize: wp('3%')
    },
    textListViewDate: {
        color: colors.colorBlack,
        fontFamily: 'ProximaNova',
        fontSize: wp('3%')
    },
    Textsukses: {
        color: colors.colorBlack,
        fontFamily: 'ProximaNova',
        fontSize: wp('4%')
    },
    TextFailed: {
        color: colors.colorsRed,
        fontFamily: 'ProximaNova',
        fontSize: wp('4%')
    },
});


const mapStateToProps = ({
    AuthReducer,
    network,
}) => {
    const { token, infoUser } = AuthReducer;
    const { isConnected } = network;
    return {
        isConnected,
        token,
        infoUser,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            dataUser,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(viewtables);
