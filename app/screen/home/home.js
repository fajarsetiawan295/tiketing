import React, { Component } from 'react';
import { View, Text, StyleSheet, YellowBox, Linking, TouchableOpacity, ScrollView, RefreshControl, Image } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { colors } from '../../conf';
import { FlatGrid } from 'react-native-super-grid';
import Icon from './../../component/icons';
import {
  Get_services
} from './../../services';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  dataUser,
  isLogged
} from './../../redux/actions';
import { api } from './../../conf/url';
import * as Progress from 'react-native-progress';

YellowBox.ignoreWarnings([
  'Calling `getNode()` on the ref of an Animated component is no longer necessary. You can now directly use the ref instead.',
  'VirtualizedLists should never be nested',
]);


class home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headItemdua: [
        {
          item: 'Tables',
          name: 'file-table-box-multiple',
          navigate: 'GetHistoryTicket',
          iconType: Icon.MaterialCommunityIcons
        },
        {
          item: 'Create Ticket',
          name: 'sticker-check-outline',
          navigate: 'CreateTicket',
          iconType: Icon.MaterialCommunityIcons
        },
      ],
      headItem: [
        {
          item: 'Create User',
          name: 'user-plus',
          navigate: 'Register',
          iconType: Icon.Feather
        },
        {
          item: 'Export Report',
          name: 'download',
          navigate: 'ExportList',
          iconType: Icon.Entypo
        },
        {
          item: 'Tables',
          name: 'file-table-box-multiple',
          navigate: 'GetHistoryTicket',
          iconType: Icon.MaterialCommunityIcons
        },
        {
          item: 'Create Ticket',
          name: 'sticker-check-outline',
          navigate: 'CreateTicket',
          iconType: Icon.MaterialCommunityIcons
        },
      ],
      dasboard: {},
      open: 0,
      onprogres: 0,
      done: 0,
    };
  }

  UNSAFE_componentWillMount() {
    this.onGetProfile()
    this.onGetDasboard()
  }

  onGetProfile() {
    try {
      Get_services(this.props.token, api.profile)
        .then((response) => {
          if (response.status == 200) {
            this.props.dataUser(response.data.data)
            console.log(response)
          }
          else if (response.status == 401) {
            console.log(response)
            this.props.navigation.navigate('Login')
          }
        })
    }
    catch (err) {
      console.log(err)

    }

  }
  onGetDasboard() {
    try {
      Get_services(this.props.token, api.GetDasboard)
        .then((response) => {
          if (response.status == 200) {
            this.setState({ dasboard: response.data.data })

            var openn = response.data.data.totalOpen / response.data.data.total * 100;
            this.setState({ open: openn / 100 })


            var onprogress = response.data.data.onprogres / response.data.data.total * 100;
            this.setState({ onprogres: onprogress / 100 })


            var donee = response.data.data.done / response.data.data.total * 100;
            this.setState({ done: donee / 100 })


            console.log(response)
          }
          else {
            console.log(response)
          }
        })
    }
    catch (err) {
      console.log(err)

    }

  }

  pressGridIcon(x) {
    const { navigation } = this.props
    if (x != '') {
      navigation.navigate(x)

    }
    else {
      Linking.openURL('https://tiketing.herokuapp.com/tiket/export_excel/')
    }
  }



  render() {
    const { navigation } = this.props
    return (
      <View>

        <View style={{
          backgroundColor: '#002bd9',
          height: wp('40%'),
        }}>
          <View style={{
            alignItems: "center",
            marginTop: hp('2%')
          }}>
            <Text style={{ color: 'white', fontSize: 20, fontWeight: "bold" }}>Welcome Dasbord</Text>
          </View>
          <View style={{ padding: wp('5%') }}>
            <Text style={{ color: 'white', fontSize: 15, fontWeight: "bold" }}>
              {this.props.infoUser.name}
            </Text>
          </View>
        </View>
        <View style={{ alignItems: "center" }}>

          <View style={styles.containerIconGrid}>
            {this.props.infoUser.status == 2 ?
              <View style={styles.subContainerIconGrid}>
                {
                  this.state.headItem.map((item, key) => {
                    return (
                      <TouchableOpacity onPress={() => this.pressGridIcon(item.navigate)} style={styles.componentGridIcon} key={key}>
                        <View style={styles.subComponentGridCyrcle}>
                          <item.iconType name={item.name} color={colors.colorsRed} size={wp('6%')} />
                        </View>
                        <Text numberOfLines={1} ellipsizeMode='tail' style={styles.subComponentGridText}>{item.item}</Text>
                      </TouchableOpacity>
                    )
                  })
                }
              </View>
              :
              <View style={{
                margin: wp('2%'),
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                width: wp('50%'),
              }}>
                {
                  this.state.headItemdua.map((item, key) => {
                    return (
                      <TouchableOpacity onPress={() => this.pressGridIcon(item.navigate)} style={styles.componentGridIcon} key={key}>
                        <View style={styles.subComponentGridCyrcle}>
                          <item.iconType name={item.name} color={colors.colorsRed} size={wp('6%')} />
                        </View>
                        <Text numberOfLines={1} ellipsizeMode='tail' style={styles.subComponentGridText}>{item.item}</Text>
                      </TouchableOpacity>
                    )
                  })
                }
              </View>
            }
          </View>
        </View>

        <View style={{
          alignItems: "center"
        }}>
          <View style={{
            alignItems: "center",
            marginTop: hp('3%'),
            borderRadius: 1,
            width: wp('90%'),
            backgroundColor: colors.colorsBorder,
            height: hp('40%')
          }}>
            <View style={{ height: hp('5%'), backgroundColor: 'white', borderWidth: 0.1 }}>
              <Text style={{
                fontWeight: "bold",
              }}>
                Statistic Ticketing Based On Open, On Progress & Done
              </Text>
            </View>
            <Text style={{
              marginTop: hp('2%'),
              fontSize: wp('5%'),
              fontWeight: "bold",
              marginBottom: hp('2%')
            }}>Open</Text>
            <Progress.Bar progress={this.state.open} height={hp('2%')} width={wp('80%')} />
            <Text style={{
              marginTop: hp('2%'),
              fontSize: wp('5%'),
              fontWeight: "bold",
              marginBottom: hp('2%')
            }}>On Progress</Text>
            <Progress.Bar progress={this.state.onprogres} color={'red'} height={hp('2%')} width={wp('80%')} />
            <Text style={{
              marginTop: hp('2%'),
              fontSize: wp('5%'),
              fontWeight: "bold",
              marginBottom: hp('2%')
            }}>Done</Text>
            <Progress.Bar progress={this.state.done} animated={true} color={'yellow'} height={hp('2%')} width={wp('80%')} />
          </View>
        </View>

        <View style={{
          flexDirection: "row",
          justifyContent: "space-around",
          marginTop: wp('5%'),
          // padding: wp('4%')
        }}>
          <View style={{
            padding: wp('1%'),
            height: hp('10%'),
            width: wp('30%'),
            borderLeftWidth: 6,
            borderLeftColor: '#001ab0',
            backgroundColor: colors.colorsBorder
          }}>
            <Text>Open Ticket</Text>
            <View style={{ width: wp('20%'), alignItems: "center", justifyContent: "center", marginTop: wp('2%') }}>
              <Text>{this.state.dasboard.totalOpen}</Text>
            </View>
          </View>
          <View style={{
            padding: wp('1%'),
            height: hp('10%'),
            width: wp('30%'),
            borderLeftWidth: 6,
            borderLeftColor: 'red',
            backgroundColor: colors.colorsBorder,
          }}>
            <Text>ON PROGRESS TICKET</Text>
            <View style={{ width: wp('20%'), alignItems: "center", justifyContent: "center", marginTop: wp('2%') }}>
              <Text>{this.state.dasboard.onprogres}</Text>
            </View>
          </View>
          <View style={{
            padding: wp('1%'),
            height: hp('10%'),
            width: wp('30%'),
            borderLeftWidth: 6,
            borderLeftColor: 'yellow',
            backgroundColor: colors.colorsBorder,
          }}>
            <Text>DONE TICKET</Text>
            <View style={{ width: wp('20%'), alignItems: "center", justifyContent: "center", marginTop: wp('2%') }}>
              <Text>{this.state.dasboard.done}</Text>
            </View>
          </View>


        </View>

      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.YellowButton,
    alignItems: "center",
  },
  componentGridIcon: {
    alignItems: 'center',
    justifyContent: 'center',
    width: wp('20%'),
  },
  subComponentGridCyrcle: {
    borderRadius: wp('20%'),
    backgroundColor: colors.colorsLight,
    height: wp('15%'),
    width: wp('15%'),
    alignItems: 'center',
    justifyContent: 'center',
  },
  subComponentGridText: {
    marginTop: 1,
    fontFamily: 'ProximaNova',
    fontSize: wp('3%'),
    color: colors.colorsLight,
    width: wp('22%'),
    alignItems: 'center',
    textAlign: 'center',
  },
  containerIconGrid: {
    width: wp('95%'),
    backgroundColor: '#B08158',
    borderRadius: 5,
    alignItems: 'center',
    marginTop: hp('-8%'),
  },
  subContainerIconGrid: {
    margin: wp('2%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: wp('90%'),
  },


})

// export default home;
const mapStateToProps = ({
  AuthReducer,
  network,
}) => {
  const { token, infoUser } = AuthReducer;
  const { isConnected } = network;
  return {
    isConnected,
    token,
    infoUser,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      dataUser,
      isLogged
    },
    dispatch,
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(home);
