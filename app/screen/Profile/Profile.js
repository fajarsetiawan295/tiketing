import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, RefreshControl } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import Icon from './../../component/icons'
import colors from '../../conf/color.global';
import { GET_PROFILE, LOGOUT, UPDATE_FOTO_PROFILE } from './../../services'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { dataUser, isLogged, tokenUser } from './../../redux/actions'
import ImagePicker from 'react-native-image-picker';
import DropdownAlert from 'react-native-dropdownalert';
import moment from 'moment';



class profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            itemContent: [],
            itemFeedback: [
                {
                    nameItem: this.props.infoUser.name,
                    iconType: Icon.MaterialCommunityIcons,
                    iconName: 'account',
                    next: ''

                },
                {
                    nameItem: this.props.infoUser.email,
                    iconType: Icon.Entypo,
                    iconName: 'email',
                    next: ''

                },
                {
                    nameItem: this.props.infoUser.departemen.departemen,
                    iconType: Icon.Entypo,
                    iconName: 'blackboard',
                    next: ''
                },
                {
                    nameItem: this.props.infoUser.location.desk_loct,
                    iconType: Icon.MaterialIcons,
                    iconName: 'location-city',
                    next: 'Kebijakan'

                },
                {
                    nameItem: this.props.infoUser.title.title,
                    iconType: Icon.MaterialIcons,
                    iconName: 'title',
                    next: 'ResetPin'

                },
                {
                    nameItem: this.props.infoUser.role.status,
                    iconType: Icon.MaterialIcons,
                    iconName: 'control-point',
                    next: 'PusatBantuan'

                }
            ],
            imageUpdate: ''
        };
    }
    onUserLogout() {
        // LOGOUT(this.props.token)
        this.props.tokenUser('logout')
        this.props.isLogged(false)
        this.props.navigation.navigate('Login')

    }
    refreshContent() {
        this.setState({ refresh: true })
        this.setState({ refresh: false })
    }


    render() {
        const state = this.state
        const { navigation, infoUser } = this.props
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <View style={styles.subContainerHeader}>
                        <Text style={styles.textHeader}>AKUN SAYA</Text>
                    </View>
                </View>
                <ScrollView
                    refreshControl={
                        <RefreshControl refreshing={this.state.refresh} onRefresh={() => this.refreshContent()} />
                    }
                    contentContainerStyle={{ alignItems: 'center' }} showsVerticalScrollIndicator={false}>

                    <View style={[styles.containerItemContent, { marginTop: hp('5%'), marginBottom: hp('4%') }]}>
                        <Text style={styles.textTitleContent}>Informasi Akun</Text>
                        {
                            this.state.itemFeedback.map((item, key) => {
                                return (
                                    <TouchableOpacity onPress={() => navigation.navigate(item.next)
                                    } key={key} style={styles.itemContent} >
                                        <item.iconType name={item.iconName} color='#1C1819' size={20} style={{ width: wp('5%') }} />
                                        <Text style={styles.itemContentText}>{item.nameItem}</Text>
                                    </TouchableOpacity>
                                )
                            })
                        }
                    </View>
                    <TouchableOpacity onPress={() => this.onUserLogout()} style={styles.buttonLogOut}>
                        <Text style={styles.textLogOut}>LOG OUT</Text>
                    </TouchableOpacity>
                </ScrollView>
                <DropdownAlert ref={(ref) => (this.dropDownAlertRef = ref)} closeInterval={5000} />
            </View >
        );
    }
}



const mapStateToProps = ({ AuthReducer, network }) => {
    const { token, infoUser } = AuthReducer
    const { isConnected } = network
    return { isConnected, token, infoUser }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ dataUser, isLogged, tokenUser }, dispatch)
}


const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#FAFAFA',
        flex: 1
    },
    headerContainer: {
        width: wp('100%'),
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: '#70707026',
        height: hp('9%'),
        marginBottom: hp('2%')
    },
    subContainerHeader: {
        flexDirection: 'row',
        width: wp('90%'),
        justifyContent: 'space-between'
    },
    textHeader: {
        color: '#1C1819',
        fontSize: wp('5%'),
        fontFamily: 'ProximaNovaSemiBold'
    },
    textTitleContent: {
        fontFamily: 'ProximaNovaSemiBold',
        fontSize: 13,
        color: '#000000'
    },
    containerUserInfo: {
        width: wp('100'),
        backgroundColor: colors.colorYellow,
        alignItems: 'center'
    },
    subContainerUserInfo: {
        width: wp('80%'),
        marginVertical: hp('2%'),
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    imageCircleUserInfo: {
        width: wp('20%'),
        height: wp('20%'),
        borderRadius: wp('10%'),
        borderWidth: 1,
        borderColor: 'red'
    },
    textNameUserInfo: {
        fontFamily: 'ProximaNovaSemiBold',
        color: colors.colorBlack,
        fontSize: 13
    },
    subTextUserInfo: {
        fontFamily: 'ProximaNova',
        color: colors.colorGrey,
        fontSize: 11
    },
    containerItemContent: {
        width: wp('80%'),
        marginTop: hp('2%')
    },
    itemContent: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: hp('2%')
    },
    itemContentText: {
        width: wp('70'),
        fontFamily: 'ProximaNova',
        color: '#383838BF',
        marginLeft: wp('5%'),
        fontSize: 13
    },
    buttonLogOut: {
        borderRadius: 5,
        borderColor: '#1C1819',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: wp('80%'),
        marginBottom: hp('5')
    },
    textLogOut: {
        marginVertical: hp('2%'),
        fontFamily: 'ProximaNovaSemiBold',
        color: '#1C1819'
    },
    componentGridIcon: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp('20%'),
    },
    subComponentGridCyrcle: {
        borderRadius: wp('20%'),
        backgroundColor: colors.colorsLight,
        height: wp('15%'),
        width: wp('15%'),
        alignItems: 'center',
        justifyContent: 'center',
    },
    subComponentGridText: {
        fontFamily: 'ProximaNova',
        fontSize: wp('3%'),
        color: colors.colorsLight,
        width: wp('22%'),
        alignItems: 'center',
        textAlign: 'center',
    },

})

export default connect(mapStateToProps, mapDispatchToProps)(profile);
