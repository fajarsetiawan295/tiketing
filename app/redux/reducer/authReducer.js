import { TYPES } from '../types'

const INITIAL_STATE = {
    isLogged: false,
    token: '',
    infoUser: {},
    error: '',
    welcome: false,
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TYPES.USER:
            return { ...state, infoUser: action.payload }
        case TYPES.TOKEN:
            return { ...state, token: action.payload }
        case TYPES.LOGGED:
            return { ...state, isLogged: action.payload }
        case TYPES.WELCOME:
            return { ...state, welcome: action.payload }
        default:
            return state
    }
}