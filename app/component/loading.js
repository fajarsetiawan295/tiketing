import React, { Component } from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { colors } from './../conf';

class Loading extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    changeValueInt(value) {
        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }

    render() {
        return (
            <View>
                <ActivityIndicator
                    animating={this.props.data}
                    color='#bc2b78'
                    size="large"
                    style={styles.activityIndicator} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    activityIndicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 80
    },
});

export default Loading;
